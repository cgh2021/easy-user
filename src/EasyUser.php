<?php

namespace EasyUser;

use EasyUser\User\User;

/**
 * Class EasyUser
 * @method static User User()   用户
 * @package sffi
 */
class EasyUser
{
    public static function make($name)
    {
        $application = "alwaystrue\\user-plug\\EasyUser\\{$name}";

        return new $application();
    }

    /**
     * Dynamically pass methods to the application.
     *
     * @param string $name
     * @param array $arguments
     *
     * @return mixed
     */
    public static function __callStatic(string $name, array $arguments)
    {
        return self::make($name);
    }
}