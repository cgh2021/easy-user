<?php
declare (strict_types = 1);

namespace EasyUser\Application;


/**
 *
 * App
 * EasyUser\App
 * CreatedBy: AlwaysTrue
 * CreatedTime: 2021/12/9 23:20
 *
 * @property \EasyUser\Application\Application $application
 *
 */
class App{}