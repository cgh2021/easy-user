<?php
declare (strict_types = 1);

namespace EasyUser\Application;

/**
 * 用户中心软件模块
 * Author: AlwaysTrue
 * Application
 * EasyUser\Application
 *
 * @method app_type()        创建应用
 * @method create($name)        创建应用
 * @method access_token()       创建应用
 * @method destroy()            销毁应用
 */
class Application{}