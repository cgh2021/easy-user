<?php
declare (strict_types = 1);

namespace EasyUser\User;


/**
 *
 * App
 * EasyUser\App
 * CreatedBy: AlwaysTrue
 * CreatedTime: 2021/12/9 23:20
 *
 * @property \EasyUser\User\User            $user
 * @property \EasyUser\User\UserInfo        $user_info
 *
 */
class App{}