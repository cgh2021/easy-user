<?php
declare (strict_types = 1);
namespace EasyUser\User;

/**
 *
 * BaseUser
 * EasyUser\BaseUser
 * CreatedBy: AlwaysTrue
 * CreatedTime: 2021/12/10 23:06
 *
 * @method create()             注册成为平台用户
 * @method valid()              检测平台账号有效性
 */
class User{}